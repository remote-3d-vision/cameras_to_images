#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <process.h>
#include <math.h>
#include <vector>

#include "CLEyeMulticam.h"
#include "image_buffer/image_buffer.cpp"

#pragma comment (lib, "CLEyeMulticam.lib")

#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480
#define IMAGE_PIX_FMT FORMAT_R8G8B8A8

using namespace std;

int main(int argc, const char** argv)
{
	char dump = 'a';
	image_buffer_writer image_writer_left;
	image_buffer_writer image_writer_right;
	image_info_t * image_info_left = new image_info_t();
	image_info_t * image_info_right = new image_info_t();


	if (image_writer_left.init_left() < 0)
	{
		printf("Failed to initialize left image buffer writer\n");
		return -1;
	}
	if (image_writer_right.init_right() < 0)
	{
		printf("Failed to initialize right image buffer writer\n");
		return -1;
	}

	image_info_left->format = FORMAT_R8G8B8A8;
	image_info_left->height = IMAGE_HEIGHT;
	image_info_left->width = IMAGE_WIDTH;
	image_info_left->size = IMAGE_WIDTH*IMAGE_HEIGHT* 4;
	image_info_right->format = FORMAT_R8G8B8A8;
	image_info_right->height = IMAGE_HEIGHT;
	image_info_right->width = IMAGE_WIDTH;
	image_info_right->size = IMAGE_WIDTH*IMAGE_HEIGHT * 4;

	const CLEyeCameraColorMode CLEYE_DEF_MODE = CLEYE_COLOR_PROCESSED;
	const CLEyeCameraResolution CLEYE_DEF_RES = CLEYE_VGA;
	const float CLEYE_DEF_RATE = 60;

	int numCams = CLEyeGetCameraCount();
	if (numCams == 0)
	{
		printf("No PS3Eye cameras detected\n");
		cin >> dump;
		return -1;
	}
	else
	{
		printf("Found %d cameras\nPress q to exit\n", numCams);
		int w, h;
		bool test, swap_outputs;
		swap_outputs = false;
		vector<CLEyeCameraInstance>::iterator it;
		vector<CLEyeCameraInstance> cams;

		//set up camera 1 with default parameters
		GUID camID = CLEyeGetCameraUUID(0);
		CLEyeCameraInstance currCam = CLEyeCreateCamera(camID, CLEYE_DEF_MODE, CLEYE_DEF_RES, CLEYE_DEF_RATE);
		test = CLEyeCameraGetFrameDimensions(currCam, w, h);
		test = CLEyeSetCameraParameter(currCam, CLEYE_GAIN, 0);
		test = CLEyeSetCameraParameter(currCam, CLEYE_EXPOSURE, 511);
		test = CLEyeSetCameraParameter(currCam, CLEYE_ZOOM, 0);
		test = CLEyeSetCameraParameter(currCam, CLEYE_ROTATION, 0);
		test = CLEyeSetCameraParameter(currCam, CLEYE_AUTO_EXPOSURE, true);
		test = CLEyeSetCameraParameter(currCam, CLEYE_AUTO_GAIN, true);
		test = CLEyeSetCameraParameter(currCam, CLEYE_AUTO_WHITEBALANCE, true);
		test = CLEyeCameraStart(currCam);
		CLEyeCameraInstance currCam1 = NULL;
		if (numCams > 1)
		{
			//since we have a second camera, set up camera 2 like camera 1
			camID = CLEyeGetCameraUUID(1);
			currCam1 = CLEyeCreateCamera(camID, CLEYE_DEF_MODE, CLEYE_DEF_RES, CLEYE_DEF_RATE);

			test = CLEyeCameraGetFrameDimensions(currCam1, w, h);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_GAIN, 0);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_EXPOSURE, 511);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_ZOOM, 0);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_ROTATION, 0);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_AUTO_EXPOSURE, true);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_AUTO_GAIN, true);
			test = CLEyeSetCameraParameter(currCam1, CLEYE_AUTO_WHITEBALANCE, true);
			test = CLEyeCameraStart(currCam1);
		}
		CLEyeCameraInstance *camera1 = &currCam, *camera2 = &currCam1;

		//required to handle keyboard input
		HANDLE stdInHandle = GetStdHandle(STD_INPUT_HANDLE);
		DWORD events;
		INPUT_RECORD keyBuffer;
		for (;;)
		{
			//Capture frames from the cameras and place them into the appropriate buffer
			//GetFrame() adds the appropriate delay for us, returning when frames are ready
			//Then we signal using write_image() that a frame is ready
			CLEyeCameraGetFrame(*camera1, image_writer_left.next_image_buffer(image_info_left));
			image_writer_left.write_image(image_info_left);
			CLEyeCameraGetFrame(*camera2, image_writer_right.next_image_buffer(image_info_right));
			image_writer_right.write_image(image_info_right);
			//handle keyboard input
			PeekConsoleInput(stdInHandle, &keyBuffer, 1, &events);
			if (events > 0)
			{
				ReadConsoleInput(stdInHandle, (PINPUT_RECORD)&keyBuffer, 1, &events);
				dump = MapVirtualKeyW(keyBuffer.Event.KeyEvent.wVirtualKeyCode, MAPVK_VK_TO_CHAR);
			}

			if (dump == 'Q')
			{

				//destroy all cams
				for (it = cams.begin(); it < cams.end(); it++)
				{
					CLEyeCameraStop(*it);
					CLEyeDestroyCamera(*it);
				}
				CLEyeCameraStop(currCam);
				CLEyeDestroyCamera(currCam);
				if (numCams > 1){
					CLEyeCameraStop(currCam1);
					CLEyeDestroyCamera(currCam1);
				}

				//exit
				return(0);
			}

		}
	}
	return 0;
}

